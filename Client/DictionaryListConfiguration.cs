using System;

namespace LanguageTrainer.Client;

public class DictionaryListConfiguration
{
    public event EventHandler NumberOfWordsUpdated;
    public bool CanCreateNewDictionary { get; set; }

    public void UpdateNumberOfWords()
    {
        NumberOfWordsUpdated?.Invoke(this, EventArgs.Empty);
    }
}