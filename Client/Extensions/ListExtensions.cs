using System;
using System.Collections.Generic;

namespace LanguageTrainer.Client.Extensions;

public static class ListExtensions
{
    private static readonly Random Random = new();
    
    public static void Shuffle<T>(this IList<T> list)  
    {  
        int n = list.Count;  
        while (n > 1) {  
            n--;  
            int k = Random.Next(n + 1);
            list.Swap(k, n);
        }
    }
    
    public static void Swap<T>(this IList<T> list, int i, int j)
    {
        (list[i], list[j]) = (list[j], list[i]);
    }
}