using LanguageTrainer.Shared;

namespace LanguageTrainer.Client;

public class SelectedDictionary
{
    public Dictionary? Dictionary { get; set; }
}