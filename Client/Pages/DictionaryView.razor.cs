using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LanguageTrainer.Shared;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace LanguageTrainer.Client.Pages;

public partial class DictionaryView
{
    [Inject] public HttpClient Http { get; set; }
        
    [CascadingParameter] public SelectedDictionary SelectedDictionary { get; set; }
    [CascadingParameter] public DictionaryListConfiguration DictionaryListConfiguration { get; set; }
        
    private readonly Regex _removeExtraWhitespace = new("\\s{2,}");
    private Dictionary? _dictionary;
    private List<Word> _dictionaryWords = new();
    private string _currentWordId;
    private string _editedWord = "";
    private string _editedTranscription;
    private string _editedTranslation;
    private readonly List<string> _translations = new();
    private ElementReference _translationElement;
    private ElementReference _wordElement;

    private bool CanSubmit => 
        _dictionary != null &&
        !string.IsNullOrEmpty(_editedWord) && 
        (_translations.Any() || _editedTranslation != null);
        
    private bool CanDelete => _currentWordId != null;
    private bool CanAdd => _editedTranslation != null;

    protected override void OnInitialized()
    {
        DictionaryListConfiguration.CanCreateNewDictionary = true;
    }

    private void AddTranslation()
    {
        _translations.Add(_editedTranslation);
        _editedTranslation = null;
    }
        
    private async Task SubmitWordAsync()
    {
        _editedWord = _removeExtraWhitespace.Replace(_editedWord, " ");
            
        using var response = await Http.PostAsJsonAsync($"/dictionaries/{_dictionary!.Id}/word", CreateRequest());
        var addWordResponse = await response.Content.ReadFromJsonAsync<AddWordResponse>();
        var word = addWordResponse!.Word;

        if (addWordResponse.Added)
        {
            _dictionaryWords.Insert(0, word);
            _dictionary.NumberOfWords++;
            DictionaryListConfiguration.UpdateNumberOfWords();
        }
        else
        {
            var existingWord = _dictionaryWords.Find(x => x.Id == _currentWordId);
            existingWord.Value = word.Value;
            existingWord.Transcription = word.Transcription;
            existingWord.Translations = word.Translations;
        }

        ClearForm();
        await _wordElement.FocusAsync();
    }

    private void ClearForm()
    {
        _currentWordId = null;
        _editedWord = null;
        _editedTranscription = null;
        _editedTranslation = null;
        _translations.Clear();
    }

    private AddWord CreateRequest()
    {
        var wordId = _currentWordId != null && _dictionaryWords.Any(x => x.Id == _currentWordId)
            ? _currentWordId
            : null;

        var translations = new List<string>(_translations);
        if (_editedTranslation != null)
            translations.Add(_editedTranslation);

        return new(wordId, _editedWord, translations, _editedTranscription, false);
    }
        
    private async Task DeleteWordAsync()
    {
        await DeleteWordAsync(_currentWordId);
    }
        
    private async Task DeleteWordAsync(string wordId)
    {
        using var response = await Http.DeleteAsync($"/dictionaries/{_dictionary!.Id}/word/{wordId}");
        response.EnsureSuccessStatusCode();

        _dictionaryWords = _dictionaryWords.Where(x => x.Id != wordId).ToList();
        _dictionary.NumberOfWords--;
        DictionaryListConfiguration.UpdateNumberOfWords();
            
        ClearForm();
    }
        
    protected override async Task OnParametersSetAsync()
    {
        if (_dictionary?.Id != SelectedDictionary.Dictionary?.Id)
        {
            _dictionary = SelectedDictionary.Dictionary;
            _dictionaryWords = _dictionary != null 
                ? await Http.GetFromJsonAsync<List<Word>>($"/dictionaries/{_dictionary.Id}/words")
                : new();
        }
    }

    private void EditTranslation(string translation)
    {
        _translations.Remove(translation);
        _editedTranslation = translation;
    }

    private void EditWord(Word word)
    {
        _currentWordId = word.Id;
        _editedWord = word.Value;
        _editedTranscription = word.Transcription;
        _editedTranslation = null;
        _translations.Clear();
        _translations.AddRange(word.Translations);
    }

    private async Task EditedWordKeyHandlerAsync(KeyboardEventArgs e)
    {
        if (e.Code == "Enter")
        {
            if (e.CtrlKey && CanSubmit)
            {
                await SubmitWordAsync();
            }

            if (_editedWord?.Length > 0) // if there is something entered, go to translation box
            {
                await _translationElement.FocusAsync();
            }
        }
    }
        
    private async Task TranslationKeyHandlerAsync(KeyboardEventArgs e)
    {
        if (e.Code == "Enter" && e.CtrlKey && CanSubmit)
        {
            await SubmitWordAsync();
        }
    }
}