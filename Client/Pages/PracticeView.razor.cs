using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LanguageTrainer.Client.Extensions;
using LanguageTrainer.Shared;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace LanguageTrainer.Client.Pages;

public partial class PracticeView
{
    enum Direction {Back, Forward}
        
    [Inject]
    private HttpClient Http { get; set; }
        
    [Inject]
    private Dictionaries Dictionaries { get; set; }
        
    [CascadingParameter] public SelectedDictionary SelectedDictionary { get; set; }
    [CascadingParameter] public DictionaryListConfiguration DictionaryListConfiguration { get; set; }
        
    private static readonly Random Random = new();
    private Dictionary? _dictionary;
    private List<Word> _originalWords = new();
    private List<Word> _words = new();
    private Word _lastWord;
    private Word _currentWord;
    private string _currentWordToTranslate;
    private string _answer;
    private int _errors;
    private int _totalErrors;
    private int _currentIndex;
    private bool _normalMode = true;
    private readonly List<string> _correctlyAnsweredIds = new();

    private bool CanGoForward => _currentIndex < _words.Count;
    private bool CanGoBack => _currentIndex > 0;
        
    protected override void OnInitialized()
    {
        DictionaryListConfiguration.CanCreateNewDictionary = false;
    }

    private async Task OnKeyDown(KeyboardEventArgs args)
    {
        if (!_normalMode)
        {
            if (args.Code == "ArrowLeft" && CanGoBack)
                GoBack();
            else if (args.Code == "ArrowRight" && CanGoForward)
                GoForward();
            else if (args.Code == "ArrowUp" && CanGoForward)
            {
                await AddToRepeatALotDictionary();
                GoForward();
            }
        }
    }

    protected override async Task OnParametersSetAsync()
    {
        if (_dictionary?.Id != SelectedDictionary.Dictionary?.Id)
        {
            _dictionary = SelectedDictionary.Dictionary;
                
            _originalWords = _dictionary != null
                ? await Http.GetFromJsonAsync<List<Word>>($"/dictionaries/{_dictionary.Id}/words")
                : new();
                
            Restart();
        }
    }

    private List<Word> ShuffleWords(List<Word> words)
    {
        var list = new List<Word>(words);
        list.Shuffle();
        foreach (var word in list)
        {
            word.Translations.Shuffle();
        }

        if (_lastWord != null && list.Count > 1 && _lastWord == list.First())
        {
            int k = Random.Next(1, list.Count - 1);
            list.Swap(0, k);
        }

        return list;
    }
        
    private async Task KeyHandler(KeyboardEventArgs e)
    {
        if (e.Code == "Enter")
        {
            _errors++;
            _totalErrors++;

            if (e.AltKey)
                await AddToRepeatALotDictionary();
                
            Next();
        }
        else
        {
            if (_answer.Trim() == _currentWord.Value)
            {
                _correctlyAnsweredIds.Add(_currentWord.Id);
                Next();
            }
        }
    }

    private void Next(Direction direction = Direction.Forward)
    {
        _lastWord = _currentWord;
            
        _currentIndex += direction == Direction.Forward ? 1 : -1;
            
        if (_currentIndex == _words.Count)
        {
            if (_normalMode && _correctlyAnsweredIds.Count < _words.Count)
            {
                _words = _words.Where(x => !_correctlyAnsweredIds.Contains(x.Id)).ToList();
                _words = ShuffleWords(_words);
                _correctlyAnsweredIds.Clear();
                _currentIndex = 0;
                _errors = 0;
                SetCurrentWordAndTranslation();
            }
            else
            {
                _currentWord = null;
                _currentWordToTranslate = null;
            }
        }
        else
        {
            SetCurrentWordAndTranslation();
        }

        _answer = "";
    }

    private async Task AddToRepeatALotDictionary()
    {
        var repeatALotDictionary = Dictionaries.GetDictionaries(SelectedDictionary.Dictionary!.Language).First(x => x.IsRepeatALot);
        var addWord = new AddWord(null, _currentWord.Value, _currentWord.Translations, _currentWord.Transcription, true);
            
        using var response = await Http.PostAsJsonAsync($"/dictionaries/{repeatALotDictionary.Id}/word", addWord);
        var addWordResponse = await response.Content.ReadFromJsonAsync<AddWordResponse>();

        if (addWordResponse!.Added)
        {
            repeatALotDictionary.NumberOfWords++;
            DictionaryListConfiguration.UpdateNumberOfWords();
        }
    }
        
    private void SetCurrentWordAndTranslation()
    {
        _currentWord = _words[_currentIndex];
        _currentWordToTranslate = _normalMode 
            ? _currentWord.Translations.First()
            : _currentWord.Value;
    }

    private void Restart()
    {
        _words = ShuffleWords(_originalWords);
        _currentIndex = -1;
        _lastWord = _currentWord = null;
        _currentWordToTranslate = null;
        _errors = _totalErrors = 0;
        _correctlyAnsweredIds.Clear();
                
        Next();
    }

    private void SwitchMode()
    {
        _normalMode = !_normalMode;
        Restart();
    }

    private void GoBack()
    {
        Next(Direction.Back);
    }
        
    private void GoForward()
    {
        Next();
    }
}