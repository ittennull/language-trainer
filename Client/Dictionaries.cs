using System.Collections.Generic;
using System.Linq;
using LanguageTrainer.Shared;

namespace LanguageTrainer.Client;

public class Dictionaries
{
    private Dictionary<Language, List<Dictionary>>? _dictionaries;

    public void SetDictionaries(IEnumerable<Dictionary> dictionaryList)
    {
        _dictionaries = dictionaryList
            .GroupBy(x => x.Language)
            .ToDictionary(x => x.Key, g => g.OrderByDescending(x => x.CreatedAt).ToList());
        
        for(var language = Language.English; language <= Language.Spanish; language++)
        {
            if (!_dictionaries.ContainsKey(language))
                _dictionaries.Add(language, new List<Dictionary>());

            var list = _dictionaries[language];
            var repeatALotIndex = list.FindIndex(x => x.IsRepeatALot);
            if (repeatALotIndex > 0)
            {
                var temp = list[repeatALotIndex];
                list.RemoveAt(repeatALotIndex);
                list.Insert(0, temp);
            }
        }
    }
        
    public void AddNewDictionary(Dictionary dictionary) =>
        _dictionaries[dictionary.Language].Insert(1, dictionary); // 1 - after "repeat a lot" dictionary

    public List<Dictionary> GetDictionaries(Language language)
    {
        if (_dictionaries == null)
            return new List<Dictionary>();
            
        _dictionaries.TryGetValue(language, out var dictionaries);
        return dictionaries;
    } 
}