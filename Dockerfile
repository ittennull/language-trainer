FROM mcr.microsoft.com/dotnet/sdk:9.0-noble
WORKDIR /build
COPY . .
RUN dotnet publish Server/LanguageTrainer.Server.csproj --output "/output" --configuration Release


FROM mcr.microsoft.com/dotnet/aspnet:9.0-noble-chiseled
WORKDIR /app
COPY --from=0 /output .
ENTRYPOINT ["dotnet", "LanguageTrainer.Server.dll"]
