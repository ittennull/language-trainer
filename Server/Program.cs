﻿using System;
using System.Threading.Tasks;
using LanguageTrainer.Server;
using LanguageTrainer.Shared;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Dictionary = LanguageTrainer.Server.Models.Dictionary;
using Word = LanguageTrainer.Server.Models.Word;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;

services.AddSingleton(new MongoDatabaseFactory().Create(builder.Configuration.GetConnectionString("MongoDb")));
AddMongoCollection<Dictionary>(services, "dictionaries");
AddMongoCollection<Word>(services, "words");

services.AddControllersWithViews();
services.AddRazorPages();

var app = builder.Build();
await EnsureRepeatALotDictionariesExistAsync(app.Services.GetRequiredService<IMongoCollection<Dictionary>>());

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseWebAssemblyDebugging();
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseBlazorFrameworkFiles();
app.UseStaticFiles();
app.UseRouting();
app.MapRazorPages();
app.MapControllers();
app.MapFallbackToFile("index.html");

app.Run();


async Task EnsureRepeatALotDictionariesExistAsync(IMongoCollection<Dictionary> collection)
{
    for(var language = Language.English; language <= Language.Spanish; language++)
    {
        var exist = await collection.AsQueryable().AnyAsync(x => x.Language == language && x.IsRepeatALot);
        if (!exist)
            await collection.InsertOneAsync(new Dictionary
            {
                Language = language,
                Name = "☆ Repeat a lot ☆",
                IsRepeatALot = true,
                CreatedAt = DateTime.UtcNow
            });
    }
}

static void AddMongoCollection<T>(IServiceCollection services, string collectionName)
{
    services.AddSingleton(
        sp => sp.GetRequiredService<IMongoDatabase>()
            .GetCollection<T>(collectionName));
}