﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LanguageTrainer.Shared;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace LanguageTrainer.Server.Controllers;

[ApiController]
[Route("[controller]")]
public class DictionariesController(
    IMongoCollection<Models.Dictionary> dictionaryCollection,
    IMongoCollection<Models.Word> wordCollection)
    : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Shared.Dictionary>>> GetDictionaries()
    {
        var dictionaries = await dictionaryCollection.AsQueryable().ToListAsync();

        var groups = await wordCollection
            .Aggregate()
            .Group(x => x.DictionaryId, x => new {x.Key, Count = x.Count()})
            .ToListAsync();

        var dictionarySizeMap = groups.ToDictionary(x => x.Key, x => x.Count);

        return Ok(dictionaries.Select(x =>
        {
            dictionarySizeMap.TryGetValue(x.Id, out var size);
            return Map(x, size);
        }));
    }
    
    [HttpPost]
    public async Task<ActionResult<Shared.Dictionary>> CreateDictionary([FromBody] AddDictionary request)
    {
        var dictionary = new Models.Dictionary
        {
            Name = request.Name,
            Language = request.Language,
            CreatedAt = DateTime.UtcNow
        };
        await dictionaryCollection.InsertOneAsync(dictionary);

        return Map(dictionary, 0);
    }
    
    [HttpGet("{dictionaryId}/words")]
    public async Task<ActionResult<IEnumerable<Shared.Word>>> GetWords(string dictionaryId)
    {
        var words = await wordCollection
            .Find(x => x.DictionaryId == dictionaryId)
            .SortByDescending(x => x.CreatedAt)
            .ToListAsync();

        return Ok(words.Select(Map));
    }
    
    [HttpPost("{dictionaryId}/word")]
    public async Task<ActionResult<AddWordResponse>> AddOrUpdateWord(string dictionaryId, [FromBody] AddWord request)
    {
        var wordItem = new Models.Word
        {
            Id = request.WordId,
            DictionaryId = dictionaryId,
            Value = request.Word.Trim(),
            Transcription = request.Transcription?.Trim(),
            Translations = request.Translations.Select(x => x.Trim()).ToArray(),
            CreatedAt = DateTime.UtcNow
        };

        bool added;
        if (request.IsRepeatALot)
        {
            var newId = ObjectId.GenerateNewId().ToString();
            wordItem = await wordCollection.FindOneAndUpdateAsync<Models.Word>(
                x => x.DictionaryId == wordItem.DictionaryId && x.Value == wordItem.Value,
                Builders<Models.Word>.Update
                    .SetOnInsert(x => x.Id, newId)
                    .Set(x => x.DictionaryId, wordItem.DictionaryId)
                    .Set(x => x.Value, wordItem.Value)
                    .Set(x => x.Transcription, wordItem.Transcription)
                    .Set(x => x.Translations, wordItem.Translations)
                    .Set(x => x.CreatedAt, DateTime.Now),
                new FindOneAndUpdateOptions<Models.Word>
                {
                    IsUpsert = true, 
                    ReturnDocument = ReturnDocument.After
                });
            added = wordItem.Id == newId;
        }
        else if (request.WordId != null)
        {
            await wordCollection.ReplaceOneAsync(x => x.Id == request.WordId, wordItem);
            added = false;
        }
        else
        {
            await wordCollection.InsertOneAsync(wordItem);
            added = true;
        }

        return new AddWordResponse(Map(wordItem), added);
    }

    [HttpDelete("{dictionaryId}/word/{id}")]
    public async Task DeleteWord(string id)
    {
        await wordCollection.DeleteOneAsync(x => x.Id == id);
    }
    
    private Shared.Word Map(Models.Word word) =>
        new ()
        {
            Id = word.Id,
            DictionaryId = word.DictionaryId,
            Value = word.Value,
            Translations = word.Translations.ToList(),
            Transcription = word.Transcription,
            CreatedAt = word.CreatedAt
        };
    
    private Shared.Dictionary Map(Models.Dictionary dictionary, int numberOfWords) =>
        new ()
        {
            Id = dictionary.Id,
            Name = dictionary.Name,
            Language = dictionary.Language,
            CreatedAt = dictionary.CreatedAt,
            NumberOfWords = numberOfWords,
            IsRepeatALot = dictionary.IsRepeatALot
        };
}
