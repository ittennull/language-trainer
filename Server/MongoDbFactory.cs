using System.Security.Authentication;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace LanguageTrainer.Server;

public class MongoDatabaseFactory
{
    public IMongoDatabase Create(string connectionString)
    {
        RegisterEnumAsStringConvention();
        
        var settings = MongoClientSettings.FromConnectionString(connectionString);
        
        settings.SslSettings = new()
        {
            EnabledSslProtocols = SslProtocols.Tls12
        };

        var mongoClient = new MongoClient(settings);
        return mongoClient.GetDatabase(MongoUrl.Create(connectionString).DatabaseName);
    }
    
    private static void RegisterEnumAsStringConvention()
    {
        var pack = new ConventionPack { new EnumRepresentationConvention(BsonType.String) };
        ConventionRegistry.Register("EnumStringConvention", pack, _ => true);
    }
}