using System;
using LanguageTrainer.Shared;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace LanguageTrainer.Server.Models;

public class Dictionary
{
    [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
    public string Id { get; init; }
    public required string Name { get; init; }
    public Language Language { get; init; }
    public DateTime CreatedAt { get; init; }
    public bool IsRepeatALot { get; set; }
}
