using System;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace LanguageTrainer.Server.Models;

public class Word
{
    [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
    public required string Id { get; init; }
    public required string DictionaryId { get; init; }
    public required string Value { get; init; }
    public required string[] Translations { get; init; }
    public string? Transcription { get; init; }
    public required DateTime CreatedAt { get; init; }
}
