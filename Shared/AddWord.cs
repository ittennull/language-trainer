using System.Collections.Generic;

namespace LanguageTrainer.Shared;

public record AddWord(
    string? WordId,
    string Word,
    ICollection<string> Translations,
    string? Transcription,
    bool IsRepeatALot);

public record AddWordResponse(Word Word, bool Added);