using System;

namespace LanguageTrainer.Shared;

public class Dictionary
{
    public required string Id { get; init; }
    public required string Name { get; init; }
    public required Language Language { get; init; }
    public DateTime CreatedAt { get; init; }
    public int NumberOfWords { get; set; }
    public bool IsRepeatALot { get; set; }
}