using System;
using System.Collections.Generic;

namespace LanguageTrainer.Shared;

public class Word
{
    public required string Id { get; init; }
    public required string DictionaryId { get; init; }
    public required string Value { get; set; }
    public required List<string> Translations { get; set; } = new(); 
    public string? Transcription { get; set; }
    public DateTime CreatedAt { get; init; }
}