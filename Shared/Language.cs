namespace LanguageTrainer.Shared;

public enum Language
{
    English,
    Dutch,
    German,
    Spanish
}