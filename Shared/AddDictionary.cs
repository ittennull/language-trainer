namespace LanguageTrainer.Shared;

public record AddDictionary(string Name, Language Language);